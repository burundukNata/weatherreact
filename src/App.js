import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Title from "./components/title";
import Form from "./components/form";
import Weather from "./components/weather";

const API_KEY="e0e84a98615f4a35847125025181311";

class App extends React.Component {
  state= {
    temperature: undefined,
    temperature_max: undefined,
    temperature_min: undefined,
    city: undefined,
    country: undefined,
    description: undefined,
    error: undefined,
    date:undefined


  }
  getWeather=async (e) => {
    e.preventDefault();
    const city=e.target.elements.city.value;
    const country=e.target.elements.country.value;
    const api_call = await fetch(`http://api.apixu.com/v1/forecast.json?key=${API_KEY}&q=${city},${country}&days=7`);



    const data = await api_call.json();

    if (city&&country) {
      console.log(data);
      this.setState({
        temperature: data.current.temp_c,
        city: data.location.name,
        country: data.location.country,
        description: data.current.condition.text,
        error: "",
        date: data.forecast.forecastday[1].date
    });


  } else {
      this.setState({
        temperature: undefined,
        temperature_max: undefined,
        temperature_min: undefined,
        city: undefined,
        country: undefined,
        description: undefined,
        error: "Please enter the value",
        date: undefined 
      })

  }
}
  render() {
    return (
      <div>
        <Title />
        <Form getWeather={this.getWeather}/>
        <Weather 
            temperature={this.state.temperature}
            temperature_max={this.state.temperature_max}
            temperature_min={this.state.temperature_min}
            city={this.state.city}
            country={this.state.country}
            description={this.state.description}
            error={this.state.error}
            date={this.state.date}

        />
      </div>
    );
  }
}




export default App;
