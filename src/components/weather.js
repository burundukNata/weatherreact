import React from "react";

const Weather = (props) => {
		return (

			<div>
				{ props.city && props.country && <p>Location: {props.city}, {props.country}</p>}			
				{ props.temperature &&	<p>Temperature: {props.temperature}</p>}
				{ props.temperature_max &&	<p>Temperature maximum: {props.temperature_max}</p>}
				{ props.temperature_min &&	<p>Temperature minimum: {props.temperature_min}</p>}
				{props.description && <p>Condition: {props.description}</p>}
				{props.error && <p>{props.error}</p>}
				{props.date && <p>Date {props.date}</p>}


			</div>
			);	
};

export default Weather;